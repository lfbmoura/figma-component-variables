module.exports = {
	emitChange(ev) {
		this.emit('change', ev);
	},
};
